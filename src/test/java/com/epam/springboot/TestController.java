package com.epam.springboot;

import com.epam.springboot.entity.User;
import com.epam.springboot.repository.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;





@SpringBootTest
@AutoConfigureMockMvc
public class TestController {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @BeforeEach
    public void setup() {
        // Aquí puedes realizar configuraciones adicionales antes de cada prueba si es necesario.
        userRepository.deleteAll();
    }

    @Test
    public void testGetAllUsers() throws Exception {
        // Agregar algunos usuarios a la base de datos de prueba.
        User user1 = new User("John", "Doe", "john@example.com");
        User user2 = new User("Jane", "Smith", "jane@example.com");
        userRepository.save(user1);
        userRepository.save(user2);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/users")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(2));
    }

    @Test
    public void testGetUserById() throws Exception {
        // Agregar un usuario a la base de datos de prueba.
        User user = new User("John", "Doe", "john@example.com");
        user = userRepository.save(user);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/" + user.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value("John"))
                .andExpect(jsonPath("$.lastName").value("Doe"))
                .andExpect(jsonPath("$.email").value("john@example.com"));
    }

    @Test
    public void testCreateUser() throws Exception {
        User user = new User("Alice", "Johnson", "alice@example.com");

        mockMvc.perform(MockMvcRequestBuilders.post("/api/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(user)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.firstName").value("Alice"))
                .andExpect(jsonPath("$.lastName").value("Johnson"))
                .andExpect(jsonPath("$.email").value("alice@example.com"));
    }

    @Test
    public void testUpdateUser() throws Exception {
        // Agregar un usuario a la base de datos de prueba.
        User user = new User("Bob", "Brown", "bob@example.com");
        user = userRepository.save(user);

        User updatedUser = new User("Updated", "User", "updated@example.com");

        mockMvc.perform(MockMvcRequestBuilders.put("/api/users/" + user.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(updatedUser)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(user.getId()))
                .andExpect(jsonPath("$.firstName").value("Updated"))
                .andExpect(jsonPath("$.lastName").value("User"))
                .andExpect(jsonPath("$.email").value("updated@example.com"));
    }

    @Test
    public void testDeleteUser() throws Exception {
        // Agregar un usuario a la base de datos de prueba.
        User user = new User("Charlie", "Chaplin", "charlie@example.com");
        user = userRepository.save(user);

        mockMvc.perform(MockMvcRequestBuilders.delete("/api/users/" + user.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        // Verificar que el usuario se haya eliminado correctamente.
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/" + user.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }





}
